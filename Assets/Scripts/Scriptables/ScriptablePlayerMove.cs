using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptablePlayerMove", menuName = "ScriptableObjects/ScriptablePlayerMove", order = 2)]
public class ScriptablePlayerMove : ScriptableObject
{
    public float speed = 6;
    public float jumpForce = 10;
    public float antiBump = 5;
    public float gravity = 25;
}