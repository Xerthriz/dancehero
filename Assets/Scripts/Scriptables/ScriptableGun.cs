using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableGun", menuName = "ScriptableObjects/ScriptableGun", order = 3)]
public class ScriptableGun : ScriptableObject
{
    public float bulletSpeed;
}
