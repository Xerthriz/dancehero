using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableAnimations", menuName = "ScriptableObjects/ScriptableAnimations", order = 1)]
public class ScriptableAnimations : ScriptableObject
{
    public string animationSelected;
    public string[] animations;
}