using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelSelected : MonoBehaviour
{
   [SerializeField]
   private Text textCurrentAnim;
   [SerializeField]
   private ScriptableAnimations scriptableAnimations;

   private void OnEnable()
   {
      ShowCurrentAnimation();
   }

   private void ShowCurrentAnimation()
   {
      textCurrentAnim.text = scriptableAnimations.animationSelected;
   }
}
