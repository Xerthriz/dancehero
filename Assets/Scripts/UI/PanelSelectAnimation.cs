using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelSelectAnimation : MonoBehaviour
{ 
   [Header("CONTROL")]
   [SerializeField] private PlayerController playerController;
   [SerializeField] private AudioController audioController;
   [SerializeField] private ButtonAnimation buttonAnimPrefab;
   
   [Header("GENERAL")]
   [SerializeField] private Transform buttonGrid;
   [SerializeField] private ScriptableAnimations scriptableAnimations;
   [SerializeField] private Button buttonNext;
   [SerializeField] private string sceneName="Game";
   private void OnEnable()
   {
      CreateAnimations();
   }

   private void CreateAnimations()
   {
       scriptableAnimations.animationSelected = string.Empty;
       
       foreach (var anim in scriptableAnimations.animations)
       {
           var btn= Instantiate(buttonAnimPrefab, buttonGrid);
           btn.textAnim.text = anim;
           btn.buttonAnim.onClick.AddListener(() => PlayAnimation(anim));
       }
       
       buttonNext.onClick.AddListener(() =>
       {
           SceneManager.LoadScene(sceneName);
       });
   }
   
   private void PlayAnimation(string anim)
   {
       playerController.ChangeAnimation(anim);
       audioController.PlayClip(anim);
       buttonNext.gameObject.SetActive(true);
       scriptableAnimations.animationSelected = anim;
   }
}
