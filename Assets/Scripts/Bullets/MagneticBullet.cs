using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MagneticBullet : MonoBehaviour
{
    [SerializeField]
    private string triggerTag = "Magnetic";
    private void OnTriggerEnter(Collider other)
    {
        if (!other.transform.CompareTag(triggerTag) || other.gameObject.GetComponent<OrbitAround>())
            return;
        
        Transform transform1;
        (transform1 = other.transform).SetParent(transform);
        transform1.position= Vector3.zero;
        other.gameObject.AddComponent<OrbitAround>().SetTarget(transform);
    }
}
