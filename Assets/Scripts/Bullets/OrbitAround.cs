using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitAround : MonoBehaviour
{

    [SerializeField]
    private float orbitDegreesPerSec = 180.0f;
    [SerializeField]
    private Vector3 relativeDistance = Vector3.zero;
    
    private Transform _target;
    
    private void LateUpdate ()
    {
        Orbit();
    }
    
    private void Orbit()
    {
        if (_target == null) 
            return;

        var position = _target.position;
        transform.position = position + relativeDistance;
        Transform transform1;
        (transform1 = transform).RotateAround(position, Vector3.up, orbitDegreesPerSec * Time.deltaTime);
        relativeDistance = transform1.position - position;
    }

    public void SetTarget(Transform tar)
    {
        _target = tar;
        var reccal = transform.position - _target.position;
        relativeDistance = new Vector3(reccal.x,0,reccal.z) ;
    }


}
