using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour
{
    [SerializeField]
    private float timeToDestroy =5;
    private void Start()
    {
        Destroy(gameObject,timeToDestroy);
    }
    
}
