using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    public void PlayClip(string clip)
    {
        audioSource.clip = Resources.Load<AudioClip>("Sounds/" + clip);
        audioSource.Play();
    }
}
