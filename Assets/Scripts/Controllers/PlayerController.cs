using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
   [SerializeField]
   private Animator animator;
   [SerializeField]
   private ScriptableAnimations scriptableAnimations;
   [SerializeField]
   private bool autoAnim;
   
   private void OnEnable()
   {
      AutoPlay();
   }

   public void ChangeAnimation(string anim)
   {
      animator.SetTrigger(anim);
   }

   private void AutoPlay()
   {
      if (autoAnim)
      {
         ChangeAnimation(scriptableAnimations.animationSelected);
      }
   }
}
