﻿using UnityEngine;

public class MouseLookController : MonoBehaviour
{

    [Header("CONFIG")]
    [SerializeField]private Vector2 clampInDegrees = new Vector2(360, 180);
    [SerializeField] private bool lockCursor;
    [SerializeField]private Vector2 sensitivity = new Vector2(2, 2);
    [SerializeField]private Vector2 smoothing = new Vector2(3, 3);
    [SerializeField]private Vector2 targetDirection;
    [SerializeField]private Vector2 targetCharacterDirection;
    [SerializeField]private GameObject characterBody;
    
    [Header("INPUTS")]
    [SerializeField] private string axiX = "Mouse X";
    [SerializeField] private string axiY = "Mouse Y";
    
    private Vector2 _mouseAbsolute;
    private Vector2 _smoothMouse;
    
    private void Start()
    {
        Setup();
    }

    private void Setup()
    {
        targetDirection = transform.localRotation.eulerAngles;

        if (characterBody)
            targetCharacterDirection = characterBody.transform.localRotation.eulerAngles;
    }

    private void LateUpdate()
    {
        if (lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        
        var targetOrientation = Quaternion.Euler(targetDirection);
        var targetCharacterOrientation = Quaternion.Euler(targetCharacterDirection);
        var mouseDelta = new Vector2(Input.GetAxisRaw(axiX), Input.GetAxisRaw(axiY));
        
        mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));
        
        _smoothMouse.x = Mathf.Lerp(_smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
        _smoothMouse.y = Mathf.Lerp(_smoothMouse.y, mouseDelta.y, 1f / smoothing.y);
        _mouseAbsolute += _smoothMouse;
        
        if (clampInDegrees.x < 360)
            _mouseAbsolute.x = Mathf.Clamp(_mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);
        
        if (clampInDegrees.y < 360)
            _mouseAbsolute.y = Mathf.Clamp(_mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);

        transform.localRotation = Quaternion.AngleAxis(-_mouseAbsolute.y, targetOrientation * Vector3.right) * targetOrientation;
        
        if (characterBody)
        {
            var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, Vector3.up);
            characterBody.transform.localRotation = yRotation * targetCharacterOrientation;
        }
        else
        {
            var yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, transform.InverseTransformDirection(Vector3.up));          
            transform.localRotation *= yRotation;
        }
    }
}
