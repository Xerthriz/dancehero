﻿using UnityEngine;
using UnityEngine.Serialization;

public class PlayerMoveController : MonoBehaviour
{
    [Header("CONTROLLERS")]
    [SerializeField] private ScriptablePlayerMove scriptablePlayerMove = null;
    [SerializeField] private CharacterController controller;
    
    [Header("INPUTS")]
    [SerializeField] private string axiX = "Horizontal";
    [SerializeField] private string axiY = "Vertical";
    [SerializeField] private string jumpKey = "Jump";
    private Vector3 _direction;
    
    private void Update()
    {
        Move();
    }

    private void FixedUpdate()
    {
        controller.Move(_direction * Time.deltaTime);
    }

    private void Move()
    {
        if (controller.isGrounded)
        {
            var x = Input.GetAxis(axiX);
            var y = Input.GetAxis(axiY);
            Vector2 input = new Vector2(x, y);

            if (input.x != 0 && input.y != 0)
            {
                input *= 0.777f;
            }

            _direction.x = input.x * scriptablePlayerMove.speed;
            _direction.z = input.y * scriptablePlayerMove.speed;
            _direction.y = -scriptablePlayerMove.antiBump;

            _direction = transform.TransformDirection(_direction);

            if (Input.GetButtonDown(jumpKey))
            {
                Jump();
            }
        }
        else
        {
            _direction.y -= scriptablePlayerMove.gravity * Time.deltaTime;
        }
    }

    private void Jump()
    {
        _direction.y += scriptablePlayerMove.jumpForce;
    }
}
