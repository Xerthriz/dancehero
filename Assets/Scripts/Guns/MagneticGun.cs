using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticGun : Gun
{
    public override void Fire()
    {
        var createdCannonball = Instantiate(bullet, barrel.position, barrel.rotation);
        createdCannonball.GetComponent<Rigidbody>().velocity = barrel.up * _speed;
    }

}
