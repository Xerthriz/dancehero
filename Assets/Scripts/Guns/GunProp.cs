using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunProp : MonoBehaviour
{
   [SerializeField]
   private Gun gunProp;
   [SerializeField]
   private string triggerTag = "Player";

   public void OnTriggerEnter(Collider other)
   {
       var guncontroller = other.GetComponent<GunController>();
       
      if (!other.CompareTag(triggerTag) || !guncontroller) 
          return;

      var gun = Instantiate(gunProp);
      guncontroller.ChangeGun(gun);
      gameObject.SetActive(false);
   }
}
