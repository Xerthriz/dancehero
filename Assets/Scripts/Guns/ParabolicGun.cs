using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolicGun : Gun
{
    public override void Fire()
    {
        var createdCannonball = Instantiate(bullet, barrel.position, barrel.rotation);
        var transform1 = barrel.transform;
        createdCannonball.GetComponent<Rigidbody>().velocity = (transform1.up + transform1.forward) * _speed;
    }

}