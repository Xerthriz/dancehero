using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadGun : Gun
{
    public override void Fire()
    {
        for (var i = -1; i < 2; i++)
        {
            var createdCannonball = Instantiate(bullet, barrel.position, barrel.rotation);
            var transform1 = barrel.transform;
            var up = transform1.up;
            createdCannonball.GetComponent<Rigidbody>().velocity = new Vector3(up.x,up.y * i ,up.z) * _speed;
        }

    }

}
