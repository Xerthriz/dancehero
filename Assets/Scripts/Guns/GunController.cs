using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    
    [SerializeField] private Gun currentGun;
    [SerializeField] private Transform gunModel;
    [SerializeField] private Transform attach;
    [SerializeField] protected string gunKey = "Fire1";
    [SerializeField] protected Vector3 posAttach =new Vector3(0.019f, 0.109f, 0.12f);
    private void Update()
    {
        Fire();
    }

    private void Fire()
    {
        if(currentGun && Input.GetButtonDown(gunKey) )
            currentGun.Fire();
    }

    public void ChangeGun(Gun gun)
    {
        Destroy(currentGun?.gameObject);
        currentGun = gun;
        Transform transform1;
        (transform1 = gun.transform).SetParent(attach.transform);
        transform1.localPosition = posAttach;
        transform1.localRotation= Quaternion.identity;
        gunModel.gameObject.SetActive(true);
    }
}
