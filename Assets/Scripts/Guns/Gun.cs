using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Gun : MonoBehaviour
{
    [SerializeField] protected ScriptableGun scriptableGun;
    [SerializeField] protected Transform barrel;
    [SerializeField] protected GameObject bullet;
    
    protected float _speed = 10;
    private void Start()
    {
        Setup();
    }
    private void Setup()
    {
        _speed = scriptableGun.bulletSpeed;
    }
    
    public virtual void Fire()
    {
        
    }
    
    
}
